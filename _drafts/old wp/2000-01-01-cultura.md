---
title: Cultura
date: '2020-05-13T13:12:00+02:00'
status: publish
permalink: /cultura
author: 'Fran Velasco'
excerpt: ''
type: page
id: 7445
---
Música, discos, museos, exposiciones, comida, arquitectura, teatro… De todo un poco.
------------------------------------------------------------------------------------

- - - - - -

<div class="wp-block-columns"><div class="wp-block-column" style="flex-basis:33.33%">##### CULTURA

</div><div class="wp-block-column" style="flex-basis:66.66%">- [Art Madrid 2018](http://franvelas.co/wp/art-madrid-2018/)<time class="wp-block-latest-posts__post-date" datetime="2018-02-25T18:00:23+01:00">25 Feb 18</time>
- [El Ángel Exterminador](http://franvelas.co/wp/el-angel-exterminador/)<time class="wp-block-latest-posts__post-date" datetime="2018-02-11T14:43:55+01:00">11 Feb 18</time>
- [Bokehlicioso!](http://franvelas.co/wp/bokehlicioso/)<time class="wp-block-latest-posts__post-date" datetime="2018-01-16T14:42:01+01:00">16 Ene 18</time>
- [Santa María](http://franvelas.co/wp/santa-maria/)<time class="wp-block-latest-posts__post-date" datetime="2015-03-05T08:47:26+01:00">5 Mar 15</time>
- [De tapitas por MadrEAT](http://franvelas.co/wp/de-tapitas-por-madreat/)<time class="wp-block-latest-posts__post-date" datetime="2015-02-15T18:31:25+01:00">15 Feb 15</time>
- [Santa María la Blanca](http://franvelas.co/wp/santa-maria-la-blanca/)<time class="wp-block-latest-posts__post-date" datetime="2015-01-17T10:12:57+01:00">17 Ene 15</time>
- [Joey Cape](http://franvelas.co/wp/joey-cape/)<time class="wp-block-latest-posts__post-date" datetime="2014-08-02T10:59:44+02:00">2 Ago 14</time>

</div></div>