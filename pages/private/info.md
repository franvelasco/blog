---
layout: page-light
title: Information
permalink: /private/info/
excerpt: A little bit of personal info.
---

<div class="accordion">

<input type="checkbox" id="Design">
<label for="Design"><h1>Design</h1></label>

<div class="content" markdown="1">
In 2002 I started working as a creative in projects related to lettering, handlettering, calligraphy, editorial design, photography and video.

In 2017, [Efless](http://efless.co){:target="_blank"} becomes my full-time studio where I continue to work with clients from all over the world, trying to improve and grow with each project.

I have had the opportunity to collaborate with music bands such as Much The Same, Desakato, Escuela de Odio, Atzembla, Acid Mess, The Attack of the Brain Eaters and many more creating artworks, posters, logos, merch designs, promos or music videos; music festivals such as Resurrection Fest, Tsunami Xixon, Caudal Fest, O Son do Camiño or Castelo Rock; drawn a series of 40 Edelvives religion school book covers; designed garments for Sevenly, The Bee & The Fox, Pampling or HRB Movement; or creating letterings, handletterings and calligraphies for different media, such as The New York Times Magazine or JOIA Magazine.

You can see my work on my website [Efless.co](http://efless.co){:target="_blank"} and in [Behance](http://behance.com/efless){:target="_blank "}, [Instagram](http://instagram.com/efless){: target =" _blank "} and [Dribbble](http://dribbble.com/efless){:target="_blank "}.
</div>




<input type="checkbox" id="Photographs">
<label for="Photographs"><h1>Photographs</h1></label>

<div class="content" markdown="1">
I take many photos (maybe too many) and sometimes I share them on [this blog]({{site.url}}/journal#Photography). Other day I remember that I'm also on [Instagram](http://instagram.com/franvelas.co){:target="_blank"} and I upload one there.

A few people have asked about the equipment I use, even if what i use is a film camera. Although it may seem so, I only shoot digital. Right now I am using a small Fujifilm X-T1 along with a 16mm f2.8, 23mm f1.4, 35mm f2 and 50mm f2 that I carry in a Billingham bag; and a Macbook Pro 13 "or iPad Pro 10.5 where I edit them using Lightroom CC.
</div>




<input type="checkbox" id="Music">
<label for="Music"><h1>Music</h1></label>

<div class="content" markdown="1">
Back in 1995 I started playing bass and until 2013 I was making noise where they left us.

I've played and/or sung on Acid Rain, Bloodguts, Plan8, DinnerDog, Some Stuff, Bah-Zillions, Eleven Fingered Boy, The Rises, A Bleeding Hero and some other bands where I don't even remember what I was doing.

You can stop by [SoundCloud](https://soundcloud.com/franvelasco/albums){:target="_blank"} and listen to some of the recordings.
</div>




<input type="checkbox" id="Mixtapes">
<label for="Mixtapes"><h1>Mixtapes</h1></label>

<div class="content" markdown="1">
Listen to my heavy usage playlists on [Spotify](https://open.spotify.com/user/efless?si=e_ZKjKO_S0uB5Ek6mhIWRA){:target="_blank"}.

[Right now](https://www.last.fm/user/efless){:target="_blank"}, I'm probably playing some soundtrack, Mac Miller, Jim Guthrie, Austin Wintory or Smashing Pumpkins. I keep waiting on a new A Wilhelm Scream album.
</div>

</div>