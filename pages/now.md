---
layout: page
title: Now
permalink: /now/

excerpt: This is my life right now.
---



Looking for new projects at [Efless](https://efless.co){:target="blank"}. Hey, [I'm available to hire!](https://efless.co/contact){:target="blank"}

---

Learning to use *Jekyll* and *Github* to make this site bigger and better.

---

Watching my son *Max*'s first steps.

---

Reading *Inspiration is the Process* by *House Industries*.

---

Watching Season Finale of *Modern Family*.